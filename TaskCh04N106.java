import java.util.Scanner;

public class TaskCh04N106 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int month = sc.nextInt();
        if (month >= 1 && month <= 12) {
            switch (month) {
                case 12:
                case 1:
                case 2:
                    System.out.println("зима");
                    break;
                case 3:
                case 4:
                case 5:
                    System.out.println("весна");
                    break;
                case 6:
                case 7:
                case 8:
                    System.out.println("лето");
                    break;
                default:
                    System.out.println("осень");
            }
        } else
            System.out.println("Введите другое число");
    }
}

