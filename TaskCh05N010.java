import java.util.Scanner;

public class TaskCh05N010 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double course = sc.nextDouble();
        int[] dol = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
        double[] rub = new double[20];
        for (int i = 0; i<dol.length; i++) {
            rub[i] = dol[i]*course;
            System.out.println(dol[i] + " $ = " + rub[i]+ " rub");
        }
    }
}
