import java.util.Scanner;

public class TaskCh10N045 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();//первый член прогресии
        double d = scanner.nextDouble();//разность
        int n = scanner.nextInt();//номер элемента
        double b = member(a, d, n);
        System.out.println(n + "-й член арифметической прогресии равен " + b);
        double summa = sum(a, d, n);
        System.out.println("Сумма " + n + " первых членов прогрессии равна " + summa);
    }

    public static double member (double a, double d, int n) {
        double result;
        if (n==1) return a;
        result = member(a, d, n-1)+d;
        return result;
    }
    public static double sum (double a, double d, int n) {
        double result = 0;
        if (n==1) return a;
        result = a + (sum (a, d, n-1)+ (n-1)*d);
        return result;
    }
}
