import java.util.Scanner;

public class TaskCh10N046 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double b = sc.nextDouble();//первый член прогрессии
        double q = sc.nextDouble();//знаменатель
        int n = sc.nextInt();
        double c = member(b, q, n);
        System.out.println(n + "й член прогрессии равен " + c);
        double d = sum(b, q, n);
        System.out.println("Сумма первых " + n + " членов прогрессии равна " + d);
    }
    public static double member (double b, double q, int n) {
        double result;
        if (n==1) return b;
        result = member(b, q, n-1)*q;
        return result;
    }
    public static double sum (double b, double q, int n) {
        double result;
        if (n==1) return b;
        result = b*Math.pow(q, n-1) + sum(b, q, n-1);
        return result;
    }
}
