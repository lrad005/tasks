import java.util.Scanner;

public class TaskCh09N042 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        StringBuilder builder = new StringBuilder(word);
        builder.reverse();//метод изменяет порядок символов на обратный
        String result = builder.toString();//преобразование в строку
        System.out.println(result);
    }
}
