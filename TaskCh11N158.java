import java.util.Scanner;

public class TaskCh11N158 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = sc.nextInt();
        }
        compare(arr, n);
        for (int i =0; i<n; i++) {
            System.out.print(arr[i] + " ");
        }

    }

    public static void compare(int[] a, int n) {
        for (int i = 0; i <n; i++) {
            for (int j = i+1; j<n; j++) {
                if (a[i] == a[j]) {
                    for (int k=j; k<n-1; k++) a[k]=a[k+1]; //смещение элементов массива влево
                    a[a.length-1]=0;//присваивание последнему элементу массива значения "0" (вместо повторяющегося элемента)
                    if (a[i]==a[j]) { //если после смещения элементов массива влево вновь получен повтор. элемент
                        a[j]=0; //присваивание значения "0" повтор. элементу
                        j++;
                    }
                }
            }
        }
    }
}

