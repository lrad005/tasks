public class TaskCh10N051 {
    public static void main(String[] args){
        int n = 5;
        pr1(n);
        System.out.println(" ");
        pr2(n);
        System.out.println(" ");
        pr3(n);
    }
    public static void pr1 (int n) {
        if (n>0) {
            System.out.print(n);
            pr1(n-1);
        }
    }
    public static void pr2(int n) {
        if (n>0) {
            pr2(n-1);
            System.out.print(n);
        }
    }
    public static void pr3(int n) {
        if (n>0) {
            System.out.print(n);
            pr3(n-1);
            System.out.print(n);
        }
    }

}

