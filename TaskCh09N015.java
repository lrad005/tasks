import java.util.Scanner;

public class TaskCh09N015 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        int length = word.length();
        int k = sc.nextInt();
        if (k<length && k>=0) {
            char result = word.charAt(k);//метод возвращает символ, расположенный по указанному индексу строки
            System.out.println(result);
        }
        else System.out.println("Неверный индекс");
    }
}

