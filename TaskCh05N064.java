import java.util.Scanner;

public class TaskCh05N064 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = 12;
        double numbersum = 0;
        double areasum = 0;
        double[] number = new double[n];
        double[] area = new double[n];
        for (int i=0; i<n; i++) {
            number[i] = sc.nextDouble();
        }
        for (int i=0; i<n; i++) {
            area[i] = sc.nextDouble();
        }
        for (int i=0; i<n; i++) {
            numbersum = numbersum + number[i];
            areasum = areasum + area[i];
        }
        double a = numbersum/areasum;
        System.out.println("Средняя плотность населения по области составляет " + a + " тыс. чел/ км^2");
    }
}

