import java.util.Scanner;

public class TaskCh10N043 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b =sum1(a);
        System.out.println("Сумма цифр равна " + b);
        int c = sum2(a);
        System.out.println("Количество цифр равно " + c);
    }
    public static int sum1 (int n) {//сумма цифрр
        int result1;
        if (n<10) return n;
        result1 = sum1(n/10) + n%10;
        return result1;
    }
    public static int sum2 (int n) {//количество цифр
        int result2;
        if (n<10) return 1;
        result2 = sum2(n/10)+1;
        return result2;
    }
}
