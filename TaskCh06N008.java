import java.util.Scanner;

public class TaskCh06N008 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int i = 1;
        while (n > (int) Math.pow(i, 2)) {
            System.out.println((int) Math.pow(i, 2));
            i++;
        }
    }
}

