import java.util.Scanner;

public class TaskCh09N107 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        CharSequence a = "а";
        CharSequence b = "о";
        if (word.contains(a)&&word.contains(b)) {
            char[] s = word.toCharArray();
            int c = word.indexOf('а');
            int d = word.lastIndexOf('о');
            char e = s[c];
            char f = s[d];
            s[c]=f;
            s[d]=e;
            for (int i=0; i<s.length; i++) System.out.print(s[i]);
        }
        else System.out.println("Такие буквы не содержатся");
    }
}

