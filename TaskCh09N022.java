import java.util.Scanner;

public class TaskCh09N022 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        int length = word.length();
        if (length % 2 == 0) {
            int a = length/2;
            String result = word.substring(0, a);//метод возвращает подстроку, начиная с индекса 0, заканчивая индексом а-1
            System.out.println(result);
        } else
            System.out.println("Слово состоит из нечетного числа букв");
    }
}
