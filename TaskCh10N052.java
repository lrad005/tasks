import java.util.Scanner;

public class TaskCh10N052 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        number(n);
    }
    public static void number (int n) {
        int res;
        if (n<10) System.out.print(n);
        else {
            number(n%10);
            number(n/10);
        }
    }
}

