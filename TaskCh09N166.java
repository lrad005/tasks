import java.util.Scanner;

public class TaskCh09N166 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String sentence = sc.nextLine();
        String[] a = sentence.split(" ");//разделяет строку вокруг символа " "
        String firstword = a[0];
        String lastword = a[a.length-1];
        a[0] = lastword;
        a[a.length-1] = firstword;
        for (int i=0; i<a.length; i++) System.out.print(a[i]+" ");
    }
}

