import java.util.Scanner;

public class TaskCh12N063 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n =11;
        int m=4;
        int[][] arr = new int[n][m];
        for (int i=0; i<n; i++){
            for (int j=0; j<m; j++){
                arr[i][j]=sc.nextInt();
            }
        }
        int sum;
        for (int i=0; i<n; i++){
            sum=0;
            for (int j=0; j<m; j++){ //для каждой строки
                sum=sum + arr[i][j];
            }
            int a =(int) Math.round((double)sum/m); //округление нецелого результата (напр, 20.5) в большую сторону
            System.out.println("Среднее количество учеников в " + (i+1) + " параллели равно " + a);
        }
    }
}

