public class OOP {

    public static void main(String[] args) {
        Candidate[] candidates = {new CandidateCourses("John"), new CandidateCourses("Mike"), new CandidateSelfTaught("Michael"),
                new CandidateCourses("Sam"), new CandidateSelfTaught("Jack"),new CandidateCourses("Harry"),
                new CandidateCourses("Oliver"), new CandidateSelfTaught("James"),
                new CandidateSelfTaught("Sam"), new CandidateSelfTaught("George")};

        Employer employer = new Employer();

        for (int i = 0; i < candidates.length; i++) {
            employer.greeting();
            candidates[i].greeting();
            candidates[i].dialog();
            System.out.println();
        }
    }

    public static class Employer implements Greeting {
        public void greeting() {
            System.out.println("Hi! Introduce yourself and describe your java experience please.");
        }
    }

    public static class CandidateCourses extends Candidate implements Greeting {
        CandidateCourses (String name) {
            super(name);
        }
        public void dialog(){
            System.out.println("I passed successfully getJavaJob exams and code reviews.");
        }
    }

    public static class CandidateSelfTaught extends Candidate implements Greeting {
        CandidateSelfTaught (String name) {
            super(name);
        }
        public void dialog(){
            System.out.println("I have been learning Java by myself, nobody examined how thorough is my knowledge and how good is my code.");
        }
    }

    abstract static class Candidate implements Greeting {
        String name;
        Candidate (String name){
            this.name=name;
        }
        abstract void dialog();
        public void greeting() {
            System.out.print("Hi! My name is " + name + ". ");
        }
    }

    public interface Greeting {
        void greeting();
    }
}

