import java.util.Scanner;

public class TaskCh10N053 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i=0; i<n; i++) {
            arr[i] = scanner.nextInt();
        }
        array(arr, n);
    }

    public static void array (int[] a, int n) {
        if (n==1) System.out.println(a[0]);
        else {
            System.out.print(a[n-1]+ " ");
            array(a, n-1);
        }
    }
}

