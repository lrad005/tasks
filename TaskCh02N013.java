import java.util.Scanner;

public class TaskCh02N013 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        if ((i>100)&&(i<200)) {
            int a = i/100; //сотни
            int b = (i%100)/10; //десятки
            int c = i%10; //единицы
            int d = c*100 + b*10 + a;
            System.out.println(d);
        }
        else {
            System.out.println("Число не входит в заданный диапазон");
        }
    }
}

