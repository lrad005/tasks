import java.util.Scanner;

public class TaskCh09N185 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String s = sc.nextLine();
        brackets(s);
        String s1 = "(ax-b)c+x+t))"; //тест
        brackets(s1);
        String s2 = "(((ax-b)c+x+t";
        brackets(s2);
    }

    public static int brackets (String s) {
        int count = 0; //счетчик
        char [] arr = s.toCharArray();
        for (int i=0; i<arr.length; i++) {
            switch (arr[i]) {
                case '(':
                    count++;
                    break;
                case ')':
                    if (count == 0) { //нет открывающих скобок, лишняя закрывающая
                        System.out.print("Нет. ");
                        System.out.println("Позиция первой лишней закрывающей скобки - " + i);//нумерация с 0
                        return i+1;
                    }
                    else count--;//уменьшаем счетчик, "обнуляя" корректную пару скобок
                    break;
            }
        }
        if (count != 0) System.out.println("Нет. Лишние открывающие скобки в количестве " + count + " штук");
        else System.out.println("Да"); //count==0
        return count;
    }
}
