public class TaskCh11N245 {
    public static void main(String[] args) {
        int[] arr = {-1, 2, 5, -8, -9, 12, 5, -7};
        int[] b = new int[arr.length];
        for (int i = 0, j = 0, k = arr.length - 1; i < arr.length; i++) {
            if (arr[i] < 0)
                b[j++] = arr[i]; // отрицательный элемент попадает в начало массива
            else
                b[k--] = arr[i]; //положительный элемент попадает в конец массива
        }

        int index = ind(b);

        for (int i=index, j=b.length-1; i<j; i++, j--) { //меняем порядок положительных элементов
            int c = b[i];
            b[i]=b[j];
            b[j]=c;
        }


        for (int i = 0; i < b.length; i++)
            System.out.print(b[i] + " ");

    }


    public static int ind (int[]a) { //индекс первого положительного элемента
        int index=0;
        for (int i=0; i<a.length; i++){
            if (a[i] >=0) index++;
        }
        return index;
    }
}
