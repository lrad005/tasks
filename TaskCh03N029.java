import java.util.Scanner;

public class TaskCh03N029 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x,y,z;
        boolean c;
        x = sc.nextInt();
        y = sc.nextInt();
        z = sc.nextInt();
        c = x%2!=0 && y%2!=0;//a
        System.out.println(c);
        c = x<20 || y<20;//б
        System.out.println(c);
        c = x==0 || y==0;//в
        System.out.println(c);
        c = x<0 && y<0 && z<0;//г
        System.out.println(c);
        c = x%5==0 || y%5==0 || z%5==0;//д
        System.out.println(c);
        c = x%5==0 && y%5!=0 && z%5!=0;//д
        System.out.println(c);
        c = x%5!=0 && y%5==0 && z%5!=0;//д
        System.out.println(c);
        c = x%5!=0 && y%5!=0 && z%5==0;//д
        System.out.println(c);
        c = x>100 || y>100 || z>100;//е
        System.out.println(c);
    }
}

