import java.util.Scanner;

public class TaskCh12N023 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int arr[][] = new int[n][n];
        mass_a(arr, n);
        print(arr, n);
        System.out.println();
        mass_b(arr, n);
        print(arr, n);
        System.out.println();
        mass_c(arr,n);
        print(arr, n);

    }

    public static void mass_a(int arr[][], int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) arr[i][j] = 1;
                else if (i+j==n-1) arr[i][j]=1;
            }
        }
    }
    public static void mass_b(int arr[][], int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) arr[i][j] = 1;//главная диагональ
                else if (i+j==n-1) arr[i][j]=1;//побочная диагональ
                else if (i==(n-1)/2) arr[i][j]=1;
                else if (j==(n-1)/2) arr[i][j]=1;
            }
        }
    }
    public static void mass_c(int arr[][], int n) {
        for (int i = 0; i <= n / 2; i++) {
            for (int j = 0; j < i; j++) {
                arr[i][j] = arr[i][n - j - 1] = 0; //строка в центре массива
            }
            for (int j = i; j < n - i; j++) {
                arr[i][j] = arr[n - i - 1][j] = 1;
            }
        }
    }
    public static void print(int arr[][], int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println();//переход на новую строку
        }
    }
}

