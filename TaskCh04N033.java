import java.util.Scanner;

public class TaskCh04N033 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();

        if (a<0) System.out.println("Число " + a +  " не является натуральным");

        else if (a%2==0) {
            System.out.println("Верно ли, что число " + a +  " заканчивается четной цифрой?");
            System.out.println("Верно");
            System.out.println("Верно ли, что число " + a +  " заканчивается нечетной цифрой?");
            System.out.println("Неверно");
        }
        else {
            System.out.println("Верно ли, что число " + a +  " заканчивается четной цифрой?");
            System.out.println("Неверно");
            System.out.println("Верно ли, что число " + a +  " заканчивается нечетной цифрой?");
            System.out.println("Верно");
        }
    }
}

