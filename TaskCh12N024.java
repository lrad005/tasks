import java.util.Scanner;

public class TaskCh12N024 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int s = 0;
        int[][] arr = new int[n][n];
        a(arr, n);
        print(arr, n);
        System.out.println();
        int [][] a = new int[n][n];
        b(a, n);
        print(a, n);
    }
    public static void a (int[][]arr, int n) {//вариант а)

        for (int i = 0, j = 0; i < n; i++) {
            arr[i][j] = 1;
        }
        for (int i = 0, j = 0; j < n; j++) {
            arr[i][j] = 1;
        }
        int a = 0;//координаты начала нового цикла
        int b = 0;

        do {
            for (int i = a, j = b; i < n - 1; i++) {
                arr[i + 1][j + 1] = arr[i + 1][j] + arr[i][j + 1];
            }
            for (int i = a, j = b; j < n - 1; j++) {
                arr[i + 1][j + 1] = arr[i + 1][j] + arr[i][j + 1];
            }
            a++;
            b++;
        }
        while (a < n && b < n);
    }

    public static void b (int[][] arr, int n) { //вариант б)
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = ((i+j)%n) + 1;
            }
        }
    }

    public static void print(int[][] arr, int n) {//вывод на экран
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                System.out.print(arr[i][j]+ " ");
            }
            System.out.println();
        }
    }
}

