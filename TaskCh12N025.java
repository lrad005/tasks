public class TaskCh12N025 {
    public static void main(String[] args) {
        int n = 12;
        int m =10;
        int[][] arr = new int[n][m];
        a(arr, n, m);
        print(arr, n, m);
        System.out.println();
        int[][] arr2 = new int[m][n];
        d(arr2, m, n);
        print(arr2, m, n);



    }
    public static void a (int[][] a, int n, int m) {
        int b=1;
        for (int i=0; i<n; i++) {
            for (int j=0; j<m; j++) {
                a[i][j]= b;
                b++;
            }
        }
    }

    public static void b (int[][] a, int n, int m) {
        int b=1;
        for (int j=0; j<m; j++) {
            for (int i=0; i<n; i++) {
                a[i][j]= b;
                b++;
            }
        }
    }

    public static void v (int[][] a, int n, int m) {
        int b=1;
        for (int i=0; i<n; i++) {
            for (int j=m-1; j>=0; j--) {
                a[i][j]= b;
                b++;
            }
        }
    }

    public static void g (int[][] a, int n, int m) {
        int b=1;
        for (int j=0; j<m; j++) {
            for (int i=n-1; i>=0; i--) {
                a[i][j]= b;
                b++;
            }
        }
    }

    public static void d (int[][] a, int n, int m) {
        int row = 0; //строка
        int b = 1;
        while (row < n) {
            if ((n - row) % 2 == 0) { //строки 0, 2, ... заполняются слева направо
                for (int i = row, j = 0; j < m; j++) {
                    a[i][j] = b;
                    b++;
                }
                row++;
            }
            else {
                for (int i = row, j = m - 1; j >= 0; j--) {
                    a[i][j] = b;
                    b++;
                }
                row++;
            }
        }
    }

    public static void e (int[][] a, int n, int m) {
        int column = 0; //столбец
        int b = 1;
        while (column < m) {
            if ((m - column) % 2 == 0) { //столбцы 0, 2, ... заполняются сверху вниз
                for (int j = column, i = 0; i < n; i++) {
                    a[i][j] = b;
                    b++;
                }
                column++;
            }
            else {
                for (int j = column, i = n - 1; i >= 0; i--) {
                    a[i][j] = b;
                    b++;
                }
                column++;
            }
        }
    }

    public static void zh (int[][] a, int n, int m) {
        int b=1;
        for (int i=n-1; i>=0; i--) {
            for (int j=0; j<m; j++) {
                a[i][j]= b;
                b++;
            }
        }
    }

    public static void z (int[][] a, int n, int m) {
        int b=1;
        for (int j=m-1; j>=0; j--) {
            for (int i=0; i<n; i++) {
                a[i][j]= b;
                b++;
            }
        }
    }

    public static void j (int[][] a, int n, int m) {
        int b=1;
        for (int i=n-1; i>=0; i--) {
            for (int j=m-1; j>=0; j--) {
                a[i][j]= b;
                b++;
            }
        }
    }

    public static void k (int[][] a, int n, int m) {
        int b=1;
        for (int j=m-1; j>=0; j--) {
            for (int i=n-1; i>=0; i--) {
                a[i][j]= b;
                b++;
            }
        }
    }

    public static void l (int[][] a, int n, int m) {
        int row = n-1; //строка
        int b = 1;
        while (row >=0) {
            if ((n - row) % 2 != 0) { //строки 1, 3, .. n-1 заполняются слева направо
                for (int i = row, j = 0; j < m; j++) {
                    a[i][j] = b;
                    b++;
                }
                row--;
            }
            else {
                for (int i = row, j = m - 1; j >= 0; j--) {
                    a[i][j] = b;
                    b++;
                }
                row--;
            }
        }
    }

    public static void m (int[][] a, int n, int m) {
        int row = 0; //строка
        int b = 1;
        while (row < n) {
            if ((n - row) % 2 != 0) { //строки 1, 3, .. n-1 заполняются слева направо
                for (int i = row, j = 0; j < m; j++) {
                    a[i][j] = b;
                    b++;
                }
                row++;
            }
            else {
                for (int i = row, j = m - 1; j >= 0; j--) {
                    a[i][j] = b;
                    b++;
                }
                row++;
            }
        }
    }

    public static void n (int[][] a, int n, int m) {
        int column = m-1; //столбец
        int b = 1;
        while (column >= 0) {
            if ((m - column) % 2 != 0) { //столбцы 1, 3, ... заполняются сверху вниз
                for (int j = column, i = 0; i < n; i++) {
                    a[i][j] = b;
                    b++;
                }
                column--;
            }
            else {
                for (int j = column, i = n - 1; i >= 0; i--) {
                    a[i][j] = b;
                    b++;
                }
                column--;
            }
        }
    }

    public static void o (int[][] a, int n, int m) {
        int column = 0; //столбец
        int b = 1;
        while (column < m) {
            if ((m - column) % 2 != 0) { //столбцы 1, 3, ... заполняются сверху вниз
                for (int j = column, i = 0; i <n; i++) {
                    a[i][j] = b;
                    b++;
                }
                column++;
            }
            else {
                for (int j = column, i = n - 1; i >= 0; i--) {
                    a[i][j] = b;
                    b++;
                }
                column++;
            }
        }
    }

    public static void p (int[][] a, int n, int m) {
        int row = n-1; //строка
        int b = 1;
        while (row >= 0) {
            if ((n - row) % 2 == 0) { //строки 0, 2, ... заполняются слева направо
                for (int i = row, j = 0; j <m; j++) {
                    a[i][j] = b;
                    b++;
                }
                row--;
            }
            else {
                for (int i = row, j = m - 1; j >= 0; j--) {
                    a[i][j] = b;
                    b++;
                }
                row--;
            }
        }
    }

    public static void r (int[][] a, int n, int m) {
        int column = m-1; //столбец
        int b = 1;
        while (column >= 0) {
            if ((m - column) % 2 == 0) { //столбцы 0, 2, ... заполняются сверху вниз
                for (int j = column, i = 0; i <n; i++) {
                    a[i][j] = b;
                    b++;
                }
                column--;
            }
            else {
                for (int j = column, i = n - 1; i >= 0; i--) {
                    a[i][j] = b;
                    b++;
                }
                column--;
            }
        }
    }

    public static void print(int[][] a, int n, int m) {
        for (int i=0; i<n; i++) {
            for (int j=0; j<m; j++) {
                System.out.print(a[i][j]+ " ");
            }

            System.out.println();
        }
    }
}


