import java.util.Scanner;

public class TaskCh10N047 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int k = sc.nextInt();
        int a = fib(k);
        System.out.println(a);
    }
    public static int fib (int k) {
        int res;
        if (k==1) return 1;
        if (k==2) return 1;
        res = fib (k-1)+fib(k-2);
        return res;
    }
}

