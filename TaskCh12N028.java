import java.util.Scanner;

public class TaskCh12N028 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[][] arr = new int[n][n];
        int start = 0;
        int finish = n;
        int m = 1;
        while (m <= n * n) { //количество элементов в массиве
            for (int j = start, i = start; j < finish; j++) { //гор.строка, слева направо
                arr[i][j] = m;
                m++;
            }
            for (int i = start+1, j = finish - 1; i < finish; i++) {//верт.строка, сверху вниз
                arr[i][j] = m;
                m++;
            }
            for (int j = finish-2, i = finish-1; j >= start; j--) {//гор.строка, справа налево
                arr[i][j] = m;
                m++;
            }
            for (int i = finish - 2, j = start; i > start; i--) {//верт.строка, снизу вверх
                arr[i][j] = m;
                m++;
            }
            start++;//уменьшение длины строк и столбцов
            finish--;
        }
        for (int i=0; i<n; i++) {
            for (int j=0; j<n; j++) {
                System.out.print(arr[i][j]+ " ");
            }
            System.out.println();
        }
    }
}
