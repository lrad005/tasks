import java.util.Scanner;

public class TaskCh04N115 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String name, col;
        String[] animal = {"Крыса", "Корова", "Тигр", "Заяц", "Дракон", "Змея", "Лошадь", "Овца", "Обезьяна", "Петух", "Собака", "Свинья"};
        String[] color = {"Зеленый", "Красный", "Желтый", "Белый", "Черный"};
        if (n >= 1984) { // а)
            int a = n - 1984;
            name = animal[a%12];
            col = color[(a%10)/2];
        }
        else { // б)
            name = animal[(n-4)%12]; //1984 % 60=4
            col = color[((n-4)%10)/2];
        }
        System.out.println(name + ". " + col);
    }
}

