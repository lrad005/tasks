import java.util.Scanner;

public class TaskCh10N049 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i=0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        int k =maxIndex(a, n);
        System.out.println(k);
        int[] b = {1, 2, 66, 6, 7};
        System.out.println(maxIndex(b, b.length));

    }
    public static int maxIndex (int[] a, int n) {
        int c;
        if (n>1) {
            c = maxIndex(a, n-1);
            if (a[n-1] < a[c]) return c;
            else return n-1;
        }
        else return n-1;
    }

}
