import java.util.Scanner;

public class TaskCh10N041 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = fact(a);
        System.out.println(b);
    }

    private static int fact(int n) {
        int result;
        if (n==1) return 1;
        result = fact(n-1)*n;
        return result;
    }
}

