import java.util.ArrayList;
import java.util.Calendar;

public class TaskCh13N012 {
    public static void main(String[] args) {
        ArrayList<Worker> list = new ArrayList<Worker>(20);
        Calendar calendar = Calendar.getInstance();
        int month_current = calendar.get(Calendar.MONTH);//текущий
        int year_current = calendar.get(Calendar.YEAR);
        Worker worker1 = new Worker("Иванов", "Иван", "Иванович", "Проспект Мира, 7", 4, 2012);
        Worker worker2 = new Worker("Петров", "Петр", "Петрович", "Пролетарская, 8", 3, 2014);
        Worker worker3 = new Worker("Петров", "Иван", "Петрович", "Пролетарская, 9", 7, 2014);
        list.add(worker1);
        list.add(worker2);
        list.add(worker3);
        for (int i=0; i<list.size(); i++) {
            int a = list.get(i).getMonth();
            int b= list.get(i).getYear();
            if ((year_current-b == 3)&&((month_current+1)-a >= 0)) list.get(i).print(); //для принятых в 2014 г.
            else if (year_current-b > 3) list.get(i).print();
        }



    }
    public static class Worker {
        String lastName, firstName, middleName;
        String address;
        int month, year;

        public Worker (String lastName, String firstName, String middleName, String address, int month, int year) {
            this.lastName = lastName;
            this.firstName = firstName;
            this.middleName = middleName;
            this.address = address;
            this.month = month;
            this.year = year;
        }

        public int getMonth() {
            return month;
        }
        public int getYear() {
            return year;
        }
        public void print(){
            System.out.println("Фамилия:"+ lastName );
            System.out.println("Имя:"+ firstName );
            System.out.println("Отчество:"+ middleName );
            System.out.println("Адрес:" + address);
            System.out.println("Дата приема: " + month + "." + year);

        }
    }
}


