import java.util.Scanner;

public class TaskCh10N048 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int a[] = new int[n];
        for (int i=0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        int max = maximum(a, n);
        System.out.println(max);
    }
    public static int maximum (int a[], int n) {
        int max;
        if (n==1) return a[0];
        else
            max = Math.max(a[n-1], maximum(a, n-1));
        return max;
    }
}

