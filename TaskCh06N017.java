import java.util.Scanner;

public class TaskCh06N017 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word = sc.nextLine();
        char first = word.charAt(0);
        int length = word.length();
        char last = word.charAt(length-1);
        if (first==last) System.out.println("Слово начинается и заканчивается на одну и ту же букву");
        else System.out.println("Первая и последняя буквы в слове различны");
    }
}

