import java.util.Scanner;

public class TaskCh10N042 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double a = sc.nextDouble();
        int b = sc.nextInt();
        System.out.println(power(a, b));
    }

    private static double power(double n, int q) {
        double result;
        if (q == 1) return n;
        result = power(n,q - 1) * n;
        return result;
    }
}

