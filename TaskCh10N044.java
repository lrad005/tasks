import java.util.Scanner;

public class TaskCh10N044 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sum(a);
        System.out.println("Цифровой корень числа " + a + " равен " +b);
    }
    public static int number (int n) {//сумма цифр числа
        int result = 0;
        while (n!=0) {
            result = result + n%10;
            n=n/10;
        }
        return result;
    }
    public static int sum (int n) {
        int summa;
        if (n < 10) return n;
        else {
            n=number(n);
            summa = sum(n);
            return summa;
        }
    }
}

