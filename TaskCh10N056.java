import java.util.Scanner;

public class TaskCh10N056 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int n = 2; // делитель
        boolean b = simple(a, n);
        System.out.println(b);
    }
    public static boolean simple (int a, int n) {// n=2, делитель, с которого начинается проверка
        if (a==2) return true;// 2- простое число, но 2%2=0
        else if (a%n==0) return false;//четное
        else if (a<2) return false;
        else if (n < a/2) //сокращаем число делителей для проверки
            return simple(a,n+1 );
        else return true;
    }
}
