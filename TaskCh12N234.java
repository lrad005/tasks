import java.util.Scanner;

public class TaskCh12N234 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int a[][] = new int[n][m];
        for (int i=0; i<n; i++) {
            for (int j=0; j<m; j++) {
                a[i][j]=sc.nextInt();
            }
        }
        int k = sc.nextInt();
        int s = sc.nextInt();
        row(a, k);//удаление строки
        print(a);
        System.out.println();
        column(a, s);//удаление столбца из массива с удаленной строкой
        print(a);

    }
    public static void row (int a[][], int k) {
        for (int i=k; i<a.length-1; i++) {
            for (int j=0; j<a[i].length; j++) {
                a[i][j]=a[i+1][j];
            }
        }
        for (int i=a.length-1, j=0; j<a[i].length; j++) {
            a[i][j]=0;
        }
    }
    public static void column (int a[][], int s) {
        for (int i=0; i<a.length; i++) {
            for (int j=s; j<a[i].length-1; j++) {
                a[i][j]=a[i][j+1];
            }
        }
        for (int i=0, j=a[i].length-1; i<a.length; i++) {
            a[i][j]=0;
        }
    }
    public static void print (int a[][]) {
        for (int i=0; i<a.length; i++) {
            for (int j=0; j<a[i].length; j++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }
}
