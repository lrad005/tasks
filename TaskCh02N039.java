import java.util.Scanner;

public class TaskCh02N039 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int h = sc.nextInt();
        int m = sc.nextInt();
        int s = sc.nextInt();
        if ((h>0 && h<=23) &&(m>=0 && m<=59)&&(s>=0 && s<=59)) {
            double r = (double) 360/(12*60*60); //угол поворота часовой стрелки за 1 сек
            double a = h*3600*r;
            double b = m*60*r;
            double c = s*r;
            double angle = a + b + c;
            System.out.println(angle);
        }
        else {
            System.out.println("Некорректные данные");
        }
    }

}

