import java.util.Scanner;

public class TaskCh10N055 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int n = sc.nextInt();
        if (n<2 || n>16) {
            System.out.println("incorrect number");
            n = sc.nextInt();
        }
        num(a, n);

    }
    public static void num (int a, int n) {
        if (a < n) {
            if (n<11) System.out.print(a%n);
            else {
                if (a%n>9) System.out.print((char)+ (a%n + 55));//необходимо, чтобы вывести "А" вместо "10" и тд, ASCII
                else System.out.print(a%n);
            }
        }

        else {
            num(a/n, n);
            if (n<11) System.out.print(a%n);
            else {
                if (a%n>9) System.out.print((char)+(a%n+55));
                else System.out.print(a%n);
            }
        }
    }
}
