import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if ((n>=100)&&(n<=999)) {
            int a = n/100; //сотни
            int b = (n%100)/10; //десятки
            int c = n%10; //единицы
            int x = a*100 + c*10 + b;
            System.out.println(x);
        }
        else {
            System.out.println("Число не входит в заданный диапазон");
        }
    }
}

