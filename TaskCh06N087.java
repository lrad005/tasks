import java.util.Scanner;

public class TaskCh06N087 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter team #1: ");
        String team1 = sc.nextLine();
        System.out.println("Enter team #2: ");
        String team2 = sc.nextLine();
        int team1_score = 0;
        int team2_score = 0;
        String winner;
        int team_number;

        do {
            System.out.println("Enter team to score (1 or 2 or 0 to finish game): ");
            team_number = sc.nextInt();
            if (team_number == 1 || team_number == 2) {
                System.out.println("Enter score (1 or 2 or 3) ");
                int score1 = sc.nextInt();
                if (team_number == 1) team1_score = team1_score + score1;
                else team2_score = team2_score + score1;
                String score = team1 + ": " + team1_score + ", " + team2 + ": " + team2_score; //intermediate score
                System.out.println("Current score: ");
                System.out.println(score);
            }
        }
        while (team_number != 0);

        if (team1_score > team2_score)
            winner = team1;
        else if (team2_score > team1_score)
            winner = team2;
        else winner = "nobody wins";
        System.out.println("Winner is " + winner);
        String result = team1 + " score is " + team1_score + "; " + team2 + " score is " + team2_score;
        System.out.println(result);
    }
}

